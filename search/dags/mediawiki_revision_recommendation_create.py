"""Ship mediawiki recommendation create events to elasticsearch

Ingests mediawiki/recommendation/create events from hive tables and writes the
set of pages that need their "recommendation exists" flag enabled to a staging
table to be picked up by the transfer_to_es dag.
"""
from datetime import datetime

from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.providers.apache.hive.operators.hive import HiveOperator
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor

from search.config.dag_config import YMDH_PARTITION, data_path, discolytics_conda_env_tgz, \
        get_default_args, eventgate_partitions
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator


dag_id = 'mediawiki_revision_recommendation_create'
var_props = VariableProperties(f'{dag_id}_config')

default_args = var_props.get_merged('default_args', {
    **get_default_args(),
    'start_date': var_props.get_datetime('start_date', datetime(2023, 3, 19)),
})

input_table = var_props.get(
    'table_event_recommendation', 'event.mediawiki_revision_recommendation_create')
output_table = var_props.get(
    'table_discovery_recommendation', 'discovery.mediawiki_revision_recommendation_create')
rel_path_discovery_recommendation = var_props.get(
    'rel_path_discovery_recommendation', 'mediawiki_revision_recommendation_create')

with DAG(
    f'{dag_id}_init',
    default_args=default_args,
    schedule_interval='@once',
) as dag_init:
    complete = DummyOperator(task_id='complete')
    HiveOperator(
        task_id='create_tables',
        hql=f"""
            CREATE TABLE IF NOT EXISTS {output_table} (
                `wikiid` string COMMENT 'MediaWiki database name',
                `page_id` int COMMENT 'MediaWiki page id',
                `page_namespace` int COMMENT 'MediaWiki namespace page_id belongs to',
                `recommendation_type` string COMMENT 'The type of the recommendation to create, typically refers to the use case'
            )
            PARTITIONED BY (
                `year` int COMMENT 'Unpadded year data collection starts at',
                `month` int COMMENT 'Unpadded month data collection starts at',
                `day` int COMMENT 'Unpadded day data collection starts at',
                `hour` int COMMENT 'Unpadded hour data collection starts at'
            )
            STORED AS PARQUET
            LOCATION '{data_path}/{rel_path_discovery_recommendation}'
        """  # noqa
    ) >> complete


with DAG(
    f'{dag_id}_hourly',
    default_args=default_args,
    schedule_interval='@hourly',
    # Allow a second job to start even if the previous is still
    # processing retrys
    max_active_runs=2,
    catchup=True,
) as dag:
    wait_for_data = NamedHivePartitionSensor(
        task_id='wait_for_data',
        mode='reschedule',
        # We send a failure email every 6 hours and keep trying for a full day.
        timeout=60 * 60 * 6,
        retries=4,
        email_on_retry=True,
        # Select single hourly partition
        partition_names=eventgate_partitions(input_table))

    # Extract the data from mediawiki event logs and put into
    # a format suitable for shipping to elasticsearch.
    extract_predictions = SparkSubmitOperator.for_virtualenv(
        task_id='extract_recommendations',
        max_executors=10,
        virtualenv_archive=discolytics_conda_env_tgz,
        entry_point='bin/prepare_recommendation_create.py',
        application_args=[
            '--input-partition', input_table + '/' + YMDH_PARTITION,
            '--output-partition', output_table + '/' + YMDH_PARTITION,
        ],
    )

    complete = DummyOperator(task_id='complete')

    wait_for_data >> extract_predictions >> complete
