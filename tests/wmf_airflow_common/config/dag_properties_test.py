from datetime import datetime, timedelta
from unittest.mock import patch

from airflow.models import Variable
from pytest import raises

from wmf_airflow_common.config.dag_properties import DagProperties

VARIABLE_NAME = "tests/wmf_airflow_common/config/dag_properties_test.py"


def test_dag_properties_with_no_prior_variable():
    with patch.object(Variable, "get", side_effect=KeyError()) as mock_get:
        with patch.object(Variable, "set") as mock_set:
            props = DagProperties(x=1, y=2)
    mock_get.assert_called_once_with(
        key=VARIABLE_NAME,
        deserialize_json=True,
    )
    mock_set.assert_called_once_with(
        key=VARIABLE_NAME,
        value={"x": 1, "y": 2},
        description=DagProperties.VARIABLE_DESCRIPTION_TEMPLATE.format(VARIABLE_NAME),
        serialize_json=True,
    )
    assert props.x == 1
    assert props.y == 2


def test_dag_properties_with_prior_variable_overrides():
    variable_body = {"x": 3, "y": 4}
    with patch.object(Variable, "get", return_value=variable_body) as mock_get:
        with patch.object(Variable, "update") as mock_update:
            props = DagProperties(x=1, y=2)
    mock_get.assert_called_once_with(
        key=VARIABLE_NAME,
        deserialize_json=True,
    )
    mock_update.assert_not_called()
    assert props.x == 3
    assert props.y == 4


def test_dag_properties_with_partial_variable_overrides():
    variable_body = {"x": 3}
    with patch.object(Variable, "get", return_value=variable_body) as mock_get:
        with patch.object(Variable, "update") as mock_update:
            props = DagProperties(x=1, y=2)
    mock_get.assert_called_once_with(
        key=VARIABLE_NAME,
        deserialize_json=True,
    )
    mock_update.assert_called_once_with(key=VARIABLE_NAME, value={"x": 3, "y": 2}, serialize_json=True)
    assert props.x == 3
    assert props.y == 2


def test_dag_properties_with_invalid_overrides():
    variable_body = {"z": 5}
    with patch.object(Variable, "get", return_value=variable_body):
        with raises(KeyError):
            DagProperties(x=1, y=2)


def test_dag_properties_with_mismatching_override_types():
    variable_body = {"x": "wrong_type"}
    with patch.object(Variable, "get", return_value=variable_body):
        with raises(ValueError):
            DagProperties(x=1)


def test_dag_properties_with_special_type_overrides():
    variable_body = {"x": "2023-01-01T00:00:00", "y": "P3D"}
    with patch.object(Variable, "get", return_value=variable_body) as mock_get:
        with patch.object(Variable, "update") as mock_update:
            props = DagProperties(x=datetime(2022, 1, 1), y=timedelta(days=3), z=3)
    mock_get.assert_called_once_with(
        key=VARIABLE_NAME,
        deserialize_json=True,
    )
    mock_update.assert_called_once_with(
        key=VARIABLE_NAME,
        value={"x": "2023-01-01T00:00:00", "y": "P3D", "z": 3},
        serialize_json=True,
    )
    assert props.x == datetime(2023, 1, 1)
    assert props.y == timedelta(days=3)
    assert props.z == 3


def test_dag_properties_with_specific_variable_name():
    variable_body = {"x": 1, "y": 2}
    with patch.object(Variable, "get", return_value=variable_body) as mock_get:
        with patch.object(Variable, "update") as mock_update:
            props = DagProperties(_variable_name="variable_name", x=1, y=2)
    mock_get.assert_called_once_with(
        key="variable_name",
        deserialize_json=True,
    )
    mock_update.assert_not_called()
    assert props.x == 1
    assert props.y == 2
