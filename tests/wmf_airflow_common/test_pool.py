from unittest.mock import mock_open, patch

from airflow.models.pool import Pool
from pytest import raises

from wmf_airflow_common.pool import PoolRegistry

yaml = """
default_pool:
  slots: 128
  description: "Default pool"

other_pool:
  slots: 1
  description: >-
    Another type of YAML string
    That allows multi lines
    """


@patch("builtins.open", mock_open(read_data=yaml))
def test_pool_registry_init():
    pool_registry = PoolRegistry(["fake_file.yaml"])
    assert len(pool_registry.pools) == 2

    default_pool = pool_registry.pools["default_pool"]
    assert default_pool["slots"] == 128
    assert default_pool["description"] == "Default pool"

    other_pool = pool_registry.pools["other_pool"]
    assert other_pool["slots"] == 1
    assert other_pool["description"] == "Another type of YAML string That allows multi lines"


@patch("builtins.open", mock_open(read_data=yaml))
def test_pool_registry_get_or_create():
    pool_registry = PoolRegistry(["fake_file.yaml"])

    with raises(KeyError):
        pool_registry.get_or_create("non_existing_pool")

    with patch.object(Pool, "create_or_update_pool") as create_or_update_pool:
        create_or_update_pool.return_value = Pool()
        assert pool_registry.get_or_create("other_pool") == "other_pool"
        assert create_or_update_pool.called
