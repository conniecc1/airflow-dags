import pytest


@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics_product', 'dags', 'wikipedia_chatgpt_plugin',
        'wikipedia_chatgpt_plugin_searches_daily_dag.py']


def test_wikipedia_chatgpt_plugin_searches_daily_dag(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id='wikipedia_chatgpt_plugin_searches_daily')
    assert dag is not None
    assert len(dag.tasks) == 2
