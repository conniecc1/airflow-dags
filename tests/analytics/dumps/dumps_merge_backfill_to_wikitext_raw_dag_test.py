import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "dumps", "dumps_merge_backfill_to_wikitext_raw_dag.py"]


def test_mediawiki_wikitext_history_to_wikitext_raw_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="dumps_merge_backfill_to_wikitext_raw")
    assert dag is not None

    assert len(dag.tasks) == 28
