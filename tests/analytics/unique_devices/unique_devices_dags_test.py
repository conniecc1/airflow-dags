import pytest


@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "unique_devices", "unique_devices_dags.py"]


def test_unique_devices_per_domain_daily_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="unique_devices_per_domain_daily")
    assert dag is not None
    assert len(dag.tasks) == 5


def test_unique_devices_per_domain_monthly_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="unique_devices_per_domain_monthly")
    assert dag is not None
    assert len(dag.tasks) == 5


def test_unique_devices_per_project_family_daily_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="unique_devices_per_project_family_daily")
    assert dag is not None
    assert len(dag.tasks) == 5


def test_unique_devices_per_project_family_monthly_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="unique_devices_per_project_family_monthly")
    assert dag is not None
    assert len(dag.tasks) == 5
