import textwrap
import warnings
from typing import Any, Optional

from airflow.exceptions import AirflowFailException

from wmf_airflow_common.operators.skein import SimpleSkeinOperator


class HDFSArchiveOperator(SimpleSkeinOperator):
    """An archiver Operator which, on HDFS:
    - checks for unicity of the file in source,
    - checks for the presence of a done file,
    - creates an archive directory with specific umask
    - moves the source file to the archive file
    - deletes the source directory
    """

    template_fields = SimpleSkeinOperator.template_fields + (
        "source_directory",
        "archive_file",
        "hdfs_tools_shaded_jar_path",
    )

    def __init__(
        self,
        source_directory: str,
        archive_file: str,
        hdfs_tools_shaded_jar_path: Optional[str] = None,
        archive_parent_umask: str = "022",
        archive_perms: str = "644",
        expected_filename_ending: str = ".gz",
        check_done: bool = False,
        done_file: str = "_SUCCESS",
        **kwargs: Any,
    ):
        """
        Creates an HDFS Archive Operator.

        :param hdfs_tools_shaded_jar_path: The path to the hdfs archiver job-shaded jar. The
        default job jar is located as an artifact in the dag_config.py of an instance.
        :param source_directory: Full hdfs path.
        :param archive_file: Full hdfs path.
        :param archive_parent_umask: The umask to create the archive (octal).
        :param archive_perms: The permission to create the archive (octal).
        :param expected_filename_ending: The end of the file name in the source directory.
        :param check_done: Is there an empty file to flag the source file as done.
        :param done_file: The name of the flag file.
        :param kwargs:
        """

        if hdfs_tools_shaded_jar_path is None:
            raise AirflowFailException(
                "Parameter 'hdfs_tools_shaded_jar_path' must be set. "
                "It should typically be defined in the Airflow instance dag_config.py."
            )

        # This operator is not idempotent: its action cannot be rerun as it depends on a previous step in the dag
        # being successful (generation of a file). As such, here we force its retries value to 0 to prevent reruns.
        if "retries" in kwargs:
            warnings.warn(
                f"User attempted to set retries={kwargs['retries']}, but HDFSArchiveOperator is not "
                f"idempotent and thus cannot retry. Forcing retries=0."
            )
        kwargs["retries"] = 0

        self.source_directory = source_directory
        self.archive_file = archive_file
        self.hdfs_tools_shaded_jar_path = hdfs_tools_shaded_jar_path

        java_classpath = "hdfs_tools_shaded_jar_path:" + "$(hadoop classpath)"

        jar_files = {"hdfs_tools_shaded_jar_path": self.hdfs_tools_shaded_jar_path}

        skein_script = f"""
                java -cp {java_classpath}
                org.wikimedia.analytics.hdfstools.HDFSArchiver
                --source_directory "{self.source_directory}"
                --archive_file "{self.archive_file}"
                --archive_parent_umask {archive_parent_umask}
                --archive_perms {archive_perms}
                --expected_filename_ending "{expected_filename_ending}"
                --check_done {str(check_done).lower()}
                --done_file "{done_file}"
            """

        skein_script = textwrap.dedent(skein_script).strip().replace("\n", " ")
        super().__init__(script=skein_script, files=jar_files, **kwargs)
