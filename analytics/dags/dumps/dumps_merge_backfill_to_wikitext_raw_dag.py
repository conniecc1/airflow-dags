"""
************
NOTICE: This job is work in progress. If it fails, do please ignore.
************

This job backfills data from source tables 'wmf.mediawiki_wikitext_history' and 'wmf_raw.mediawiki_revision'
into target table wmf_dumps.wikitext_raw_rc1, an Iceberg table meant to be the base to create dumps from.

We accomplish this by running two PySpark jobs. The first one joins data from the source tables above
and writes it to an intermediate, temporary table with an ideal partitioning schema for the next step.
The second PySpark job reads from the intermediate table and runs a MERGE INTO
that updates a particular (wiki_db, revision_id) tuple if the target table watermark is
older than the source watermark.

More info about the pyspark jobs at:
https://gitlab.wikimedia.org/repos/data-engineering/dumps/mediawiki-content-dump/-/blob/main/mediawiki_content_dump/backfill_create_intermediate_table.py
https://gitlab.wikimedia.org/repos/data-engineering/dumps/mediawiki-content-dump/-/blob/main/mediawiki_content_dump/backfill_merge_into.py
"""
import getpass
from datetime import datetime, timedelta

from analytics.config.dag_config import (
    artifact,
    create_easy_dag,
    hadoop_name_node,
    pool,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator, SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor

props = DagProperties(
    # DAG settings
    start_date=datetime(2023, 7, 1),
    current_year=2023,  # unfortunately, it seems we need to set this manually to make tests happy.
    sla=timedelta(days=7),
    conda_env=artifact("mediawiki-content-dump-0.1.0.dev0-fix-canary-for-visbility.conda.tgz"),
    # target table
    hive_wikitext_raw_table="wmf_dumps.wikitext_raw_rc2",
    # source tables
    hive_mediawiki_wikitext_history_table="wmf.mediawiki_wikitext_history",
    hive_mediawiki_revision_table="wmf_raw.mediawiki_revision",
    # intermediate table tmp folder
    temporary_folder=f"{hadoop_name_node}/wmf/tmp/analytics/dumps",
    # Spark job tuning
    driver_memory="32G",
    driver_cores="4",
    executor_memory="20G",
    executor_cores="2",
    num_executors="100",
    spark_executor_memoryOverhead="4G",
    # avoid FetchFailed exceptions as much as possible
    spark_sql_shuffle_partitions="131072",
    spark_shuffle_io_retryWait="15s",
    spark_shuffle_io_maxRetries="15",
    spark_network_timeout="600s",
    # maxResultSize default is 1g, and its giving us problems with MERGE INTO tasks
    spark_driver_maxResultSize="8G",
    # extra settings as per https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Cluster/Spark#Executors
    spark_shuffle_file_buffer="1m",
    spark_shuffle_service_index_cache_size="256m",
    spark_io_compression_lz4_blockSize="512KB",
    # this job runs Spark 3.3. Disable shuffle service for now as 3.1's is incompatible
    spark_shuffle_service_enabled="False",
    spark_dynamicAllocation_enabled="False",
    spark_sql_adaptive_coalescePartitions_enabled="True",
    # Configs for running very simple SparkSQL code from the Skein app.
    minimalist_spark_config={
        "master": "local",
        "driver_cores": 1,
        "driver_memory": "1G",
    },
)

with create_easy_dag(
    dag_id="dumps_merge_backfill_to_wikitext_raw",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=[
        "monthly",
        "from_hive",
        "to_iceberg",
        "requires_wmf_mediawiki_wikitext_history",
        "requires_wmf_raw_mediawiki_revision",
        "uses_spark",
        "uses_sql",
        "mediawiki_dumps",
    ],
    sla=props.sla,
    max_active_runs=1,
    max_active_tasks=1,  # MERGE INTOs use lots of resources, let's limit it to 1 concurrent per group
    email="xcollazo@wikimedia.org",  # overriding alert email for now.
    # it's ok if this fails, and we don't want to alert ops week folk.
) as dag:
    snapshot = "{{data_interval_start | to_ds_month}}"
    wikitext_sensor = URLSensor(
        task_id="wait_for_data_in_mw_wikitext_history",
        url=f"{hadoop_name_node}/wmf/data/wmf/mediawiki/wikitext/history/snapshot={snapshot}/_PARTITIONED",
        poke_interval=timedelta(hours=1).total_seconds(),
    )
    revision_sensor = URLSensor(
        task_id="wait_for_data_in_raw_mediawiki_revision",
        url=f"{hadoop_name_node}/wmf/data/raw/mediawiki/tables/revision/snapshot={snapshot}/_PARTITIONED",
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    # temporary table that partitions the source data in an ideal way for backfilling
    intermediate_table = "tmp.tmp_{{dag.dag_id}}_{{data_interval_start | to_ds_nodash}}"
    intermediate_table_location = props.temporary_folder + "/{{dag.dag_id}}/{{data_interval_start | to_ds_nodash}}"

    # Usage: backfill_create_intermediate_table.py --backfill_source_table backfill_source
    #                                              --visibility_source_table visibility_source
    #                                              --intermediate_table intermediate_target
    #                                              --snapshot 2023-06
    create_intermediate_table_args = [
        "--backfill_source_table",
        props.hive_mediawiki_wikitext_history_table,
        "--visibility_source_table",
        props.hive_mediawiki_revision_table,
        "--intermediate_table",
        intermediate_table,
        "--intermediate_table_location",
        intermediate_table_location,
        "--snapshot",
        snapshot,
    ]

    username = getpass.getuser()
    common_spark_conf = {
        "spark.executor.memoryOverhead": props.spark_executor_memoryOverhead,
        "spark.sql.shuffle.partitions": props.spark_sql_shuffle_partitions,
        "spark.shuffle.io.retryWait": props.spark_shuffle_io_retryWait,
        "spark.shuffle.io.maxRetries": props.spark_shuffle_io_maxRetries,
        "spark.network.timeout": props.spark_network_timeout,
        "spark.driver.maxResultSize": props.spark_driver_maxResultSize,
        "spark.shuffle.file.buffer": props.spark_shuffle_file_buffer,
        "spark.shuffle.service.index.cache.size": props.spark_shuffle_service_index_cache_size,
        "spark.io.compression.lz4.blockSize": props.spark_io_compression_lz4_blockSize,
        "spark.shuffle.service.enabled": props.spark_shuffle_service_enabled,
        "spark.dynamicAllocation.enabled": props.spark_dynamicAllocation_enabled,
        "spark.sql.adaptive.coalescePartitions.enabled": props.spark_sql_adaptive_coalescePartitions_enabled,
        "spark.jars.packages": "org.apache.iceberg:iceberg-spark-runtime-3.3_2.12:1.2.1",
        # avoid ivy errors by making cache location unique per service user
        "spark.driver.extraJavaOptions": f"-Divy.cache.dir=/tmp/{username}/ivy_spark3/cache -Divy.home=/tmp/{username}/ivy_spark3/home",  # noqa
        "spark.jars.ivySettings": "/etc/maven/ivysettings.xml",  # fix jar pulling
        "spark.yarn.archive": "hdfs:///user/spark/share/lib/spark-3.3.2-assembly.zip",  # override 3.1's assembly
    }

    create_intermediate_table = SparkSubmitOperator.for_virtualenv(
        task_id="spark_create_intermediate_table",
        virtualenv_archive=props.conda_env,
        entry_point="bin/backfill_create_intermediate_table.py",
        driver_memory=props.driver_memory,
        driver_cores=props.driver_cores,
        executor_memory=props.executor_memory,
        executor_cores=props.executor_cores,
        num_executors=props.num_executors,
        conf=common_spark_conf,
        launcher="skein",
        application_args=create_intermediate_table_args,
        use_virtualenv_spark=True,
        default_env_vars={
            "SPARK_HOME": "venv/lib/python3.10/site-packages/pyspark",  # point to the packaged Spark
            "SPARK_CONF_DIR": "/etc/spark3/conf",
        },
    )

    # Usage: backfill_merge_into.py --intermediate_table intermediate_source_table
    #                               --target_table target_table
    #                               --snapshot 2023-06
    #                               --years 2021
    merge_into_partial_args = [
        "--intermediate_table",
        intermediate_table,
        "--target_table",
        props.hive_wikitext_raw_table,
        "--snapshot",
        snapshot,
    ]

    years = range(2001, props.current_year + 1)  # generate years since beginning of wiki time to now

    # generate one MERGE INTO per year
    merge_intos = []
    for year in years:
        merge_into = SparkSubmitOperator.for_virtualenv(
            task_id=f"spark_backfill_merge_into_{year}",
            virtualenv_archive=props.conda_env,
            entry_point="bin/backfill_merge_into.py",
            driver_memory=props.driver_memory,
            driver_cores=props.driver_cores,
            executor_memory=props.executor_memory,
            executor_cores=props.executor_cores,
            num_executors=props.num_executors,
            conf=common_spark_conf,
            launcher="skein",
            application_args=merge_into_partial_args + ["--years", year],
            use_virtualenv_spark=True,
            default_env_vars={
                "SPARK_HOME": "venv/lib/python3.10/site-packages/pyspark",  # point to the packaged Spark
                "SPARK_CONF_DIR": "/etc/spark3/conf",
            },
            # This pool with 1 slot allows us to have multiple DAGs with
            # MERGE INTOs that effectively run serially against table "wmf_dumps.wikitext_raw", thus mimicking a mutex.
            pool=pool("merge_into_mutex_for_wmf_dumps_wikitext_raw"),
            # bump priority of this task (from default of 1) so that backfill tasks are preferred over other MERGEs
            priority_weight=5,
        )
        merge_intos.append(merge_into)

    drop_intermediate_table = SparkSqlOperator(
        task_id="drop_intermediate_table",
        sql=f"DROP TABLE IF EXISTS {intermediate_table};",
        **props.minimalist_spark_config,
    )

    remove_intermediate_files = SimpleSkeinOperator(
        task_id="remove_intermediate_files",
        script=f"hdfs dfs -rm -r -skipTrash {intermediate_table_location}",
    )

    (
        wikitext_sensor
        >> revision_sensor
        >> create_intermediate_table
        >> merge_intos
        >> drop_intermediate_table
        >> remove_intermediate_files
    )
