from datetime import datetime

from analytics.dags.webrequest.refine_webrequest_hourly_dag_factory import generate_dag

for webrequest_source in ["text", "upload"]:
    # https://github.com/apache/airflow/blob/2.5.1/airflow/utils/file.py#L358
    generate_dag(  # This generates an Airflow DAG
        dag_id=f"refine_webrequest_hourly_{webrequest_source}",
        webrequest_source=webrequest_source,
        default_start_date=datetime(2023, 4, 11, 14),
        tags=["hourly", "uses_hql", "uses_email", "from_hive", "to_hive", "requires_wmf_raw_webrequest"],
    )
