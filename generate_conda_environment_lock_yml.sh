#!/usr/bin/env bash

set -e
set -x

now="$(date +%s)"
env_name="tmp_env_${now}"

source_file="conda-environment.yml"
lock_file="conda-environment.lock.yml"

if [ -z "$(command -v conda)" ]; then
  echo "conda is not in your path"
  exit 1
else
  conda --version
fi

conda env create --name $env_name -f $source_file

rm -f $lock_file

# appnope is MacOS Specific
conda env export --no-builds -n $env_name | \
  grep -v prefix | \
  grep -v name | \
  grep -v appnope \
  > $lock_file

git --no-pager diff $lock_file

conda env remove --name $env_name

set +x

echo "${lock_file} created !
note:
  - sometimes Conda is introducing dependency bugs (e.g. see graphviz). Do run the check script.
  - don't commit the git+url lines or replace them with their url"

